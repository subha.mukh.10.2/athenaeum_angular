import { Component, ViewEncapsulation } from '@angular/core';
import * as AOS from 'aos'

@Component({
  selector: 'app-newly-added',
  templateUrl: './newly-added.component.html',
  styleUrls: ['./newly-added.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class NewlyAddedComponent {
  ngOnInit() {
    AOS.init()
  }
}
