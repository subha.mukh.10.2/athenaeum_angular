import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NavbarComponent } from './navbar/navbar.component';
import { CarouselHomepageComponent } from './carousel-homepage/carousel-homepage.component';
import { NewlyAddedComponent } from './newly-added/newly-added.component';

@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    CarouselHomepageComponent,
    NewlyAddedComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
